#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSvgWidget>
#include <QGraphicsView>
#include <QGraphicsSvgItem>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setSVG(const QString &file);

public slots:
    void anim();

private:
    Ui::MainWindow *ui;
    QSvgWidget *svgWidget;
    int dt;
    int it;
};

#endif // MAINWINDOW_H
