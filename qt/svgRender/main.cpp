#include "mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QDesktopWidget dw;
    MainWindow w;
    int x=dw.width()*0.3;
    int y=dw.height()*0.8;
    w.setFixedSize(x,y);
    w.show();

    //w.setSVG("../../svg/frame23.svg");
    /*int dt = 0;
    QString file;
    while(dt < 24)
    {
        file = "../../svg/frame"+QString::number(dt)+".svg";
        //qDebug(file.toLatin1());
        w.setSVG(file);
        dt ++;
        Sleep(1000/24);
    }*/

    return a.exec();
}
