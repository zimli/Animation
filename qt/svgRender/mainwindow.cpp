#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    svgWidget = new QSvgWidget;
    svgWidget->setUpdatesEnabled(true);
    setCentralWidget(svgWidget);
    dt = 0;
    it = 1;

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(anim()));
    timer->setSingleShot(false);
    timer->start(1000/24);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setSVG(const QString &file)
{
    svgWidget->load(file);
    //svgWidget->repaint();
}

void MainWindow::anim()
{
    setSVG("../../svg/frame"+QString::number(dt)+".svg");

    if (dt == 23)
    {
        it = -1;
    }
    else if (dt == 0)
    {
        it = 1;
    }
    dt += it;
}
