import sys
import re
from xml.dom import minidom
from shutil import copyfile

def parse(svg_file):
    doc = minidom.parse(svg_file)
    path_id = [path.getAttribute('id') for path
                    in doc.getElementsByTagName('path')]
    path_d = [path.getAttribute('d') for path
                    in doc.getElementsByTagName('path')]
    doc.unlink()
    
    return (path_id, path_d)

#A point-to-point linear interpolation
def interpolate(path1, path2, v):
    path3 = ""
    coord1 = re.split(', | |,', path1)
    coord2 = re.split(', | |,', path2)
    #MoveTo(M or m) should be the first
    path3 += "M " + str(float(coord1[1]) + v * (float(coord2[1]) - float(coord1[1])))
    path3 += "," + str(float(coord1[2]) + v * (float(coord2[2]) - float(coord1[2])))
    #set the iterator starting at 3, skip M or m and 2 numbers
    it1 = 3
    it2 = 3
    rx1 = coord1[1]
    ry1 = coord1[2]
    rx2 = coord2[1]
    ry2 = coord2[2]
    while it1 < len(coord1) and it2 < len(coord2):
        if coord1[it1] == 'Z' or coord2[it2] == 'Z':
            break
        
        if coord1[it1] == 'c':
            for i in range(it1+1, it1+7):
                if (i-it1)%2 == 1:
                    coord1[i] = str(float(coord1[i]) + float(rx1))
                else:
                    coord1[i] = str(float(coord1[i]) + float(ry1))
            coord1[it1] = 'C'
            
        if coord2[it2] == 'c':
            for i in range(it2+1, it2+7):
                if (i-it2)%2 == 1:
                    coord2[i] = str(float(coord2[i]) + float(rx2))
                else:
                    coord2[i] = str(float(coord2[i]) + float(ry2))
            coord2[it2] = 'C'
            
        if coord1[it1] == 'C' and coord2[it2] == 'C':
            path3 += " C " + str(float(coord1[it1+1]) + v * (float(coord2[it2+1]) - float(coord1[it1+1]))) + "," + str(float(coord1[it1+2]) + v * (float(coord2[it2+2]) - float(coord1[it1+2])))
            path3 += " " + str(float(coord1[it1+3]) + v * (float(coord2[it2+3]) - float(coord1[it1+3]))) + "," + str(float(coord1[it1+4]) + v * (float(coord2[it2+4]) - float(coord1[it1+4])))
            path3 += " " + str(float(coord1[it1+5]) + v * (float(coord2[it2+5]) - float(coord1[it1+5]))) + "," + str(float(coord1[it1+6]) + v * (float(coord2[it2+6]) - float(coord1[it1+6])))
            rx1 = coord1[it1+5]
            ry1 = coord1[it1+6]
            rx2 = coord2[it2+5]
            ry2 = coord2[it2+6]
            it1 += 7
            it2 += 7
            continue
        elif coord1[it1] != 'C':
            path3 += " C " + str(float(coord1[it1]) + v * (float(coord2[it2+1]) - float(coord1[it1]))) + "," + str(float(coord1[it1+1]) + v * (float(coord2[it2+2]) - float(coord1[it1+1])))
            path3 += " " + str(float(coord1[it1+2]) + v * (float(coord2[it2+3]) - float(coord1[it1+2]))) + "," + str(float(coord1[it1+3]) + v * (float(coord2[it2+4]) - float(coord1[it1+3])))
            path3 += " " + str(float(coord1[it1+4]) + v * (float(coord2[it2+5]) - float(coord1[it1+4]))) + "," + str(float(coord1[it1+5]) + v * (float(coord2[it2+6]) - float(coord1[it1+5])))
            rx1 = coord1[it1+4]
            ry1 = coord1[it1+5]
            rx2 = coord2[it2+5]
            ry2 = coord2[it2+6]
            it1 += 6
            it2 += 7
            continue
        elif coord2[it2] != 'C':
            path3 += " C " + str(float(coord1[it1+1]) + v * (float(coord2[it2]) - float(coord1[it1+1]))) + "," + str(float(coord1[it1+2]) + v * (float(coord2[it2+1]) - float(coord1[it1+2])))
            path3 += " " + str(float(coord1[it1+3]) + v * (float(coord2[it2+2]) - float(coord1[it1+3]))) + "," + str(float(coord1[it1+4]) + v * (float(coord2[it2+3]) - float(coord1[it1+4])))
            path3 += " " + str(float(coord1[it1+5]) + v * (float(coord2[it2+4]) - float(coord1[it1+5]))) + "," + str(float(coord1[it1+6]) + v * (float(coord2[it2+5]) - float(coord1[it1+6])))
            rx1 = coord1[it1+5]
            ry1 = coord1[it1+6]
            rx2 = coord2[it2+4]
            ry2 = coord2[it2+5]
            it1 += 7
            it2 += 6
            continue
                
    return path3

def write(root, file, paths):
    f = open(file, "w")
    for src_path in root.getElementsByTagName('path'):
        for dst_path in paths:
            if str(dst_path[0]) == src_path.getAttribute('id'):
                src_path.attributes['d'].value = dst_path[1]
  
    root.writexml(f)
    f.close()

def main():
    #root = minidom.parse("../svg/expression6.svg")
    #out1 = parse("../svg/expression6.svg")
    #out2 = parse("../svg/expression7.svg")
    #count = 3
    root = minidom.parse(sys.argv[1])
    out1 = parse(sys.argv[1])
    out2 = parse(sys.argv[2])
    count = int(sys.argv[3])
    step = 1.0/float(count)
    for k in range(count):
        out3 = []
        for i in range(len(out1[0])):
            for j in range(len(out2[0])):
                if out1[0][i] == out2[0][j]:
                    out3.append((out1[0][i], interpolate(out1[1][i], out2[1][j], k*step)))
                    break  
        write(root, "../svg/frame" + str(k) + ".svg", out3)
    root.unlink()
    
if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Usage: svg_parser svg1 svg2 count_of_frames")
        sys.exit(0)
    main()
