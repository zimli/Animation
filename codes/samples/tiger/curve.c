const int curveCommandCount = 6;
const char curveCommands[6] = {
	'N', 'S', 'B', 'M', 'M', 'C'
};

const float curveMinX = 0.0f;
const float curveMaxX = 640.0f;
const float curveMinY = 0.0f;
const float curveMaxY = 480.0f;

const int curvePointCount = 17;
const float curvePoints[17] = {
	10, 1, 0.1f, 0.1f, 0.1f, 0.8f, 0.8f, 0.8f, 2,
	70, 110, 70, 260, 310, 260, 310, 110
};