#ifndef __CURVE_H
#define __CURVE_H

extern const int curveCommandCount;
extern const char curveCommands[];
extern const float curveMinX;
extern const float curveMaxX;
extern const float curveMinY;
extern const float curveMaxY;
extern const int curvePointCount;
extern const float curvePoints[];

#endif